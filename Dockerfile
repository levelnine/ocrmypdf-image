FROM php:8.1-fpm-bullseye

ENV OCRMYPDF_VERSION="v14.0.3"

ENV LANG=C.UTF-8
ENV TZ=UTC
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update -y && apt-get install -y --no-install-recommends \
    autoconf \
    automake \
    build-essential \
    ca-certificates \
    curl \
    ghostscript \
    git \
    gpg-agent \
    img2pdf \
    jbig2dec \
    libasound2 \
    libatk1.0-0 \
    libcups2 \
    libffi-dev \
    libfreetype6-dev \
    libgbm-dev \
    libgconf-2-4 \
    libgtk-3-0 \
    libjpeg62-turbo-dev \
    liblept5 \
    libleptonica-dev \
    libnss3 \
    libonig-dev \
    libpangocairo-1.0-0 \
    libpng-dev \
    libqpdf-dev \
    libsm6 \
    libtool \
    libx11-xcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxi6 \
    libxml2-dev \
    libxrandr2 \
    libxrender-dev \
    libxss1 \
    libxtst6 \
    libzip-dev \
    openssl \
    pngquant \
    python3 \
    python3-dev \
    python3-distutils \
    software-properties-common \
    tesseract-ocr \
    tesseract-ocr-eng \
    tesseract-ocr-spa \
    unpaper \
    unzip \
    zip \
    zlib1g \
    zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install bcmath zip pdo pdo_mysql opcache soap exif intl
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

COPY php-fpm-logging.conf /usr/local/etc/php-fpm.d/logging.conf

RUN pecl install -o -f redis \
    &&  docker-php-ext-enable redis

RUN curl https://bootstrap.pypa.io/get-pip.py | python3

# Compile and install jbig2
# Needs libleptonica-dev, zlib1g-dev
RUN \
  mkdir jbig2 \
  && curl -L https://github.com/agl/jbig2enc/archive/ea6a40a.tar.gz | \
  tar xz -C jbig2 --strip-components=1 \
  && cd jbig2 \
  && ./autogen.sh && ./configure && make && make install \
  && cd .. \
  && rm -rf jbig2

WORKDIR /ocrmypdf

RUN pip3 install --no-cache-dir git+https://github.com/ocrmypdf/OCRmyPDF@$OCRMYPDF_VERSION
